/*
 * SPI testing utility (using spidev driver)
 *
 * Copyright (c) 2007  MontaVista Software, Inc.
 * Copyright (c) 2007  Anton Vorontsov
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * Cross-compile with cross-gcc -I/path/to/cross-kernel/include
 *
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

static void pabort(const char *s) {
	perror(s);
	abort();
}

static const char *device = "/dev/spidev1.0";
static uint8_t mode = 3;
static uint8_t bits = 8;
static uint32_t speed = 2000000;
static uint16_t delay;
int fd;

uint8_t tx[] = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};
uint8_t rx[] = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};


// SONOMA Register Locations
#define SONOMA_ADDR_CMD        0x00	// [int ] Command Register (see Command Register section)
#define SONOMA_ADDR_FWDATE     0x01	// [int ] Firmware release date in hex format (0x00YMDD)
#define SONOMA_ADDR_SAMPLES    0x08	// [int ] High-Rate Samples per Low Rate (default 400)
#define SONOMA_S0_GAIN         0x0D	// [S.21] Input S0 Gain Calibration. Positive values only
#define SONOMA_S2_GAIN         0x0F	// [S.21] Input S2 Gain Calibration. Positive values only
#define SONOMA_ADDR_VA_RMS	   0x2B	// [S.23] RMS Voltage for VA source
#define SONOMA_ADDR_VA_FUND	   0x2D	// [S.23] Fundamental Voltage for VA source
#define SONOMA_ADDR_VA_HARM    0x2F	// [S.23] Harmonic Voltage for VA source
#define SONOMA_ADDR_VA         0x33	// [S.23] Instantaneous Voltage for VA source
#define SONOMA_ADDR_UART_BAUD  0xB1	// [int ] Baud rate for UART interface (Default 38400)

volatile uint32_t param[10];

float CalculaS21(){

	float retorno = 0;

	retorno += rx[2] & 64 ? 2 : 0; // bit da posição 22
	retorno += rx[2] & 32 ? 1 : 0; // bit da posição 21
	retorno += rx[2] & 16 ? 0.5 : 0; // 1/2
	retorno += rx[2] & 8  ? 0.25 : 0; // 1/4
	retorno += rx[2] & 4  ? 0.125 : 0; // 1/8
	retorno += rx[2] & 2  ? 0.0625 : 0; // 1/16
	retorno += rx[2] & 1  ? 0.03125 : 0; // 1/32

	retorno += rx[3] & 128 ? 0.015625 : 0; // 1/64
	retorno += rx[3] & 64  ? 0.0078125 : 0; // 1/128
	retorno += rx[3] & 32  ? 0.00390625 : 0; // 1/256
	retorno += rx[3] & 16  ? 0.001953125 : 0; // 1/512
	retorno += rx[3] & 8   ? 0.0009765625 : 0; // 1/1024

	retorno = rx[2] & 128 ? (-1) * retorno : retorno; // Verifica o sinal

	return retorno;
}

float CalculaS23(){

	float retorno = 0;

	retorno += rx[2] & 64 ? 0.5 : 0; // 1/2
	retorno += rx[2] & 32 ? 0.25 : 0; // 1/4
	retorno += rx[2] & 16 ? 0.125 : 0; // 1/8
	retorno += rx[2] & 8  ? 0.0625 : 0; // 1/16
	retorno += rx[2] & 4  ? 0.03125 : 0; // 1/32
	retorno += rx[2] & 2  ? 0.015625 : 0; // 1/64
	retorno += rx[2] & 1  ? 0.0078125 : 0; // 1/128

	retorno += rx[3] & 128 ? 0.00390625 : 0; // 1/256
	retorno += rx[3] & 64  ? 0.001953125 : 0; // 1/512
	retorno += rx[3] & 32  ? 0.0009765625 : 0; // 1/1024
	retorno += rx[3] & 16  ? 0.00048828125 : 0; // 1/2048
	retorno += rx[3] & 8   ? 0.000244140625 : 0; // 1/4096

	retorno = rx[2] & 128 ? (-1) * retorno : retorno; // Verifica o sinal

	return retorno;
}

uint32_t CalculaInt(){

	uint32_t retorno;

	retorno = rx[4] + (rx[3] << 8) + (rx[2] << 16);

	return retorno;
}

static void transfer() {
	int ret;

	struct spi_ioc_transfer tr = { .tx_buf = (unsigned long) tx, .rx_buf =
			(unsigned long) rx, .len = ARRAY_SIZE(tx), .delay_usecs = delay,
			.speed_hz = 0, .bits_per_word = 0, };

	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
	if (ret == 1)
		pabort("can't send spi message");

	for (ret = 0; ret < ARRAY_SIZE(tx); ret++) {
		printf("0x%.2X ", rx[ret]);
	}
	puts("");
}

/**
 * \brief       Reads the value of a register
 * \par         Details
 *
 * \param[in]   uchRegAddr      - Register Address
 *
 * \retval      Register Value
 */
static void spiRead(uint8_t uchRegAddr) {

	//preparing read command
	tx[0] = uchRegAddr >> 6;
	tx[0] = (tx[0] << 2) + 1;
	tx[1] = 0x3F & uchRegAddr;
	tx[1] = tx[1] << 2;
	tx[2] = 0;
	tx[3] = 0;
	tx[4] = 0;
	tx[5] = 0;

	transfer();

}

void spiWrite(uint8_t uchRegAddr, uint32_t unRegValue) {

	//preparing write command
	tx[0] = uchRegAddr >> 6;
	tx[0] = (tx[0] << 2) + 1;
	tx[1] = 0x3F & uchRegAddr;
	tx[1] = (tx[1] << 2) + 2;
	tx[2] = unRegValue >> 16;
	tx[3] = unRegValue >> 8;
	tx[4] = unRegValue;
	tx[5] = 0;

	transfer();

}

void print_usage(const char *prog) {
	printf("Usage: %s [-DsbdlHOLC3]\n", prog);
	puts("  -D --device   device to use (default /dev/spidev1.1)\n"
			"  -s --speed    max speed (Hz)\n"
			"  -d --delay    delay (usec)\n"
			"  -b --bpw      bits per word \n"
			"  -l --loop     loopback\n"
			"  -H --cpha     clock phase\n"
			"  -O --cpol     clock polarity\n"
			"  -L --lsb      least significant bit first\n"
			"  -C --cs-high  chip select active high\n"
			"  -3 --3wire    SI/SO signals shared\n");
	exit(1);
}

void parse_opts(int argc, char *argv[]) {
	while (1) {
		static const struct option lopts[] = { { "device", 1, 0, 'D' }, {
				"speed", 1, 0, 's' }, { "delay", 1, 0, 'd' },
				{ "bpw", 1, 0, 'b' }, { "loop", 0, 0, 'l' },
				{ "cpha", 0, 0, 'H' }, { "cpol", 0, 0, 'O' },
				{ "lsb", 0, 0, 'L' }, { "cs-high", 0, 0, 'C' }, { "3wire", 0, 0,
						'3' }, { NULL, 0, 0, 0 }, };
		int c;

		c = getopt_long(argc, argv, "D:s:d:b:lHOLC3", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
		case 'D':
			device = optarg;
			break;
		case 's':
			speed = atoi(optarg);
			break;
		case 'd':
			delay = atoi(optarg);
			break;
		case 'b':
			bits = atoi(optarg);
			break;
		case 'l':
			mode |= SPI_LOOP;
			break;
		case 'H':
			mode |= SPI_CPHA;
			break;
		case 'O':
			mode |= SPI_CPOL;
			break;
		case 'L':
			mode |= SPI_LSB_FIRST;
			break;
		case 'C':
			mode |= SPI_CS_HIGH;
			break;
		case '3':
			mode |= SPI_3WIRE;
			break;
		default:
			print_usage(argv[0]);
			break;
		}
	}
}

int main(int argc, char *argv[]) {
	printf("Iniciando Teste. Projeto IOT.");

	int ret = 0;

	parse_opts(argc, argv);

	fd = open(device, O_RDWR);
	if (fd < 0)
		pabort("can't open device");

	/*
	 * spi mode
	 */
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		pabort("can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		pabort("can't get spi mode");

	/*
	 * bits per word
	 */
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		pabort("can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		pabort("can't get bits per word");

	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		pabort("can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		pabort("can't get max speed hz");

	printf("spi mode: %d\n", mode);
	printf("bits per word: %d\n", bits);
	printf("max speed: %d Hz (%d KHz)\n", speed, speed / 1000);

	//leio o comando
	//spiRead(SONOMA_ADDR_CMD);

	//leio o firmware update
	//spiRead(SONOMA_ADDR_FWDATE);

	//escrevo o firmware update
	//spiWrite(SONOMA_ADDR_FWDATE, 0x005927);

	//delay_ms(1);
	//leio o firmware update
	//spiRead(SONOMA_ADDR_FWDATE);

	printf("[Teste] Leitura do Baudrate (Default 38400):\n");
	sleep(1);
	spiRead(SONOMA_ADDR_UART_BAUD);
	printf("Valor decimal = %d \n\n",CalculaInt());

	printf("Leitura de VA_RMS:\n");
	sleep(1);
	spiRead(SONOMA_ADDR_VA_RMS);
	printf("S.23: Valor = %f \n\n", CalculaS23()/0.0015 );

	printf("Leitura de S0_GAIN:\n");
	sleep(1);
	spiRead(SONOMA_S0_GAIN);
	printf("S.21: Valor = %f \n\n", CalculaS21());

	printf("Leitura de S2_GAIN:\n");
	sleep(1);
	spiRead(SONOMA_S2_GAIN);
	printf("S.21: Valor = %f \n\n", CalculaS21());

	close(fd);

	return ret;
}

